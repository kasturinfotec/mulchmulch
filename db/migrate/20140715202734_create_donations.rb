class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.integer :total_contribution
      t.float :updated_contribution
      t.string :frequency
      t.string :payment_method
      t.integer :transaction_id, unique: true
      t.string :status
      t.date :payment_date
      t.integer :children_id
      t.integer :user_id

      t.timestamps
    end
  end
end
