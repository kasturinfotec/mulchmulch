class CreateGuardians < ActiveRecord::Migration
  def change
    create_table :guardians do |t|
      t.string :first_nm
      t.string :last_nm
      t.date :dob
      t.string :address
      t.string :phone
      t.integer :child_id

      t.timestamps
    end
  end
end
