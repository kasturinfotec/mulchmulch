class CreateContributions < ActiveRecord::Migration
  def change
    create_table :contributions do |t|
      t.integer :child_id
      t.integer :user_id
      t.integer :frequency_id
      t.integer :total_contribution
      t.integer :payment_id

      t.timestamps
    end
  end
end
