class CreateChildrenDonators < ActiveRecord::Migration
  def change
    create_table :children_donators do |t|
      t.integer :children_id
      t.integer :donator_id

      t.timestamps
    end
  end
end
