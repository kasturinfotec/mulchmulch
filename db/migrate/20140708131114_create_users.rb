class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :role
      t.string :first_nm
      t.string :last_nm
      t.date :dob
      t.string :appt_no
      t.string :st_no
      t.string :city
      t.string :country
      t.string :state
      t.string :zipcode
      t.string :ref_id, :unique => true
      t.string :profile
      t.string :phone
      t.boolean :iaccept, default: false

      t.timestamps
    end
  end
end
