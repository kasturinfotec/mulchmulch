class RegistrationsController < Devise::RegistrationsController

	def new
		@ref = User.find_by(:ref_id=>params[:ref_id]) 
		super
	end

	def create
		unless params[:user][:referral_id].blank?
			@ref = User.find(params[:user][:referral_id]) rescue nil
			params[:ref_id] = @ref.ref_id rescue nil
		end
		params[:user][:ref_id] = nil
		super
	end
  	
  	protected

  		def after_sign_up_path_for(resource)
			UsersMailer.welcome_message(resource).deliver
			flash.delete(:notice)
			if session[:ref_id].nil?
				update_profile_path
			else
				"/pledge-now?ref_id=#{session[:ref_id]}"
			end
		end
end