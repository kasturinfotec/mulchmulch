ActiveAdmin.register User do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :first_nm,:first_nm,:dob ,:gender
  #


  index do
    selectable_column
    id_column
    column :ref_id
    column :first_nm
    column :first_nm
    column :gender
    column :dob
    actions
  end


   filter :first_nm
   filter :first_nm
   filter :gender
   filter :dob
  # or 
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
