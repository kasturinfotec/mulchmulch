# == Schema Information
#
# Table name: guardians
#
#  id         :integer          not null, primary key
#  first_nm   :string(255)
#  last_nm    :string(255)
#  dob        :date
#  address    :string(255)
#  phone      :string(255)
#  child_id   :integer
#  created_at :datetime
#  updated_at :datetime
#  category   :string(255)
#  appt_no    :string(255)
#  st_no      :string(255)
#  city       :string(255)
#

class Guardian < ActiveRecord::Base
	belongs_to :child, class_name: 'User'	
end
