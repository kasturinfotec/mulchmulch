# == Schema Information
#
# Table name: cups
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  price      :float
#  image      :string(255)
#  created_at :datetime
#  updated_at :datetime
#  cup_image  :string(255)
#

class Cup < ActiveRecord::Base
	has_many :donation_cup, dependent: :destroy
	has_many :donations, through: :donation_cup
end
