# == Schema Information
#
# Table name: donations
#
#  id                   :integer          not null, primary key
#  total_contribution   :integer
#  updated_contribution :float
#  frequency            :string(255)
#  payment_method       :string(255)
#  transaction_id       :integer
#  status               :string(255)
#  payment_date         :date
#  children_id          :integer
#  user_id              :integer
#  created_at           :datetime
#  updated_at           :datetime
#

class Donation < ActiveRecord::Base
	has_many :donation_cup, dependent: :destroy
	has_many :cups, through: :donation_cup
end