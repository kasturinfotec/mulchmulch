# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  role                   :string(255)
#  first_nm               :string(255)
#  last_nm                :string(255)
#  dob                    :date
#  appt_no                :string(255)
#  st_no                  :string(255)
#  city                   :string(255)
#  country                :string(255)
#  state                  :string(255)
#  zipcode                :string(255)
#  ref_id                 :string(255)
#  profile                :string(255)
#  phone                  :string(255)
#  iaccept                :boolean          default(FALSE)
#  created_at             :datetime
#  updated_at             :datetime
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  username               :string(255)
#  avatar_file_name       :string(255)
#  avatar_content_type    :string(255)
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  mobile                 :string(255)
#  secondary_email        :string(255)
#  age_role               :string(255)
#  referral_id            :integer
#  ssn                    :string(255)
#  parent_id              :integer
#  gender                 :string(255)
#

require 'securerandom'
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,:omniauthable,
       :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:login]
  # attr_encrypted :ssn, :key => '03d4f676c501896bf6498f5498b461568970c4e626b15a2720c6c8b4e060182c244b58d8e4f6d397a028e65980b27611e5e30964a8c4178804dd5114c621e8aa'
  validates_inclusion_of :gender, :in => ['male', 'female'], :allow_nil => true, :message => "%{value} is not gender"
  mount_uploader :profile, AvatarUploader
  before_create :set_ref_id

  has_attached_file :avatar,
    :storage => :dropbox,
    :dropbox_credentials => { app_key: Figaro.env.dropbox_app_key, app_secret: Figaro.env.dropbox_app_secret, access_token: Figaro.env.dropbox_access_token, access_token_secret: Figaro.env.dropbox_access_token_secret, user_id: Figaro.env.dropbox_user_id, access_type: Figaro.env.dropbox_access_type }

  validates_attachment_content_type :avatar, content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  has_many :referrals, :class_name => "User", foreign_key: "referral_id"
  belongs_to :reference, :class_name => "User"
  has_many :guardians, foreign_key: :child_id
  accepts_nested_attributes_for :guardians
  after_create :generate_guardians
  has_many :children, class_name: "User", foreign_key: "parent_id"
  belongs_to :parent, class_name: "User"
  
  has_many :child_donators, class_name: "ChildrenDonator", :foreign_key => "children_id"
  has_many :donators, through: :child_donators
  
  has_many :donator_childs, class_name: "ChildrenDonator"
  has_many :childs, through: :donator_childs, :foreign_key => "donator_id"
  
  scope :all_children, -> { where(role: 'children') }
  
  def self.next_page_count children
    return (children.current_page < children.total_pages) ? children.current_page + 1 : 0
  end

  def full_name
    name = self.first_nm rescue ""
    name = "#{name} #{self.last_nm}" rescue ""
    name.blank? ? email_to_name : name.titlecase
  end

  def email_to_name
    name = self.email[/[^@]+/]
    name.split(".").map {|n| n.capitalize }.join(" ")
  end

  def alias_name
    name = (!self.first_nm.nil? || !self.last_nm.nil?) ? get_name : self.email_to_name.titlecase
    name
  end

  def get_name
    name = ""
    if !self.first_nm.nil?
      name = self.first_nm.titlecase + " "
    end
    if !self.last_nm.nil?
      name += self.last_nm.titlecase
    end
    name
  end

  def self.generate_new_email first_nm, last_nm
    "#{first_nm}.#{last_nm}#{Time.now.to_i}@mulch.com"
  end
  
  def ref_count
    User.where(:referral_id=>self.id).count
  end

  private

    def set_ref_id
      found = false
      while not found
  	    self.ref_id ||= SecureRandom.hex(3).upcase
  			found = !User.exists?(:ref_id => self.ref_id)
  		end
    end

  def generate_guardians
    self.guardians.create(category: 'father')
    self.guardians.create(category: 'second')
  end

  TEMP_EMAIL_PREFIX = 'change@me'
  TEMP_EMAIL_REGEX = /\Achange@me/



  validates_format_of :email, :without => TEMP_EMAIL_REGEX, on: :update
def self.find_for_google_oauth2(access_token, signed_in_resource=nil)
    data = access_token.info
    user = User.where(:email => data["email"]).first

    # Uncomment the section below if you want users to be created if they don't exist
    # unless user
    #     user = User.create(name: data["name"],
    #        email: data["email"],
    #        password: Devise.friendly_token[0,20]
    #     )
    # end
    user
end

  def self.find_for_oauth(auth, signed_in_resource = nil)

    # Get the identity and user if they exist
    identity = Identity.find_for_oauth(auth)

    # If a signed_in_resource is provided it always overrides the existing user
    # to prevent the identity being locked with accidentally created accounts.
    # Note that this may leave zombie accounts (with no associated identity) which
    # can be cleaned up at a later date.
    user = signed_in_resource ? signed_in_resource : identity.user

    # Create the user if needed
    if user.nil?

      # Get the existing user by email if the provider gives us a verified email.
      # If no verified email was provided we assign a temporary email and ask the
      # user to verify it on the next step via UsersController.finish_signup
      email_is_verified = auth.info.email && (auth.info.verified || auth.info.verified_email)
      email = auth.info.email if email_is_verified
      user = User.where(:email => email).first if email

      # Create the user if it's a new registration
      if user.nil?
        user = User.new(
          username: auth.extra.raw_info.name,
          #username: auth.info.nickname || auth.uid,
          email: email ? email : "#{TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
          password: Devise.friendly_token[0,20]
        )
        user.skip_confirmation! if user.respond_to?(:skip_confirmation)
        user.save!
      end
    end
if auth.provider == 'google_oauth2'
        email = auth.info.email
      else
        email_is_verified = auth.info.email && (auth.info.verified || auth.info.verified_email)
        email = auth.info.email if email_is_verified
      end
    # Associate the identity with the user if needed
    if identity.user != user
      identity.user = user
      identity.save!
    end
    user
  end

  def email_verified?
    self.email && self.email !~ TEMP_EMAIL_REGEX
  end

end
